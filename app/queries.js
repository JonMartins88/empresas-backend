const Pool = require('pg').Pool
const pool = new Pool({
    user: process.env['DB_USER'],
    host: process.env['DB_HOST'],
    database: process.env['DATABASE'],
    password: process.env['DB_PASSWORD'],
    port: 5432,
    // user: "me",
    // host: "localhost",
    // database: "empresas-db",
    // password: "GdLv0003",
    // port: 5432,
})

const getEmpresas = (request, response) => {
    pool.query('SELECT * FROM empresas ORDER BY id ASC', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

const getEmpresaById = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('SELECT * FROM empresas WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

const createEmpresa = (request, response) => {
    const { name } = request.body

    pool.query('INSERT INTO empresas (name) VALUES ($1)', [name], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`Empresa adicionada com ID: ${results.insertid}`)
    })
}

const updateEmpresa = (request, response) => {
    const id = parseInt(request.params.id)
    const { name } = request.body

    pool.query(
        'UPDATE empresas SET name = $1 WHERE id = $3',
        [name, id],
        (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).send(`Empresa modificada com ID: ${results.id}`)
        }
    )
}

const deleteEmpresa = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('DELETE FROM empresas WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).send(`Empresa deletada com ID: ${results.id}`)
    })
}

module.exports = {
    getEmpresas,
    getEmpresaById,
    createEmpresa,
    updateEmpresa,
    deleteEmpresa,
}