const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000
require('dotenv').config({path: __dirname + '/.env'})

const db = require('./queries')

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express and Postgres API'})
})
app.get('/api/v1/enterprises', db.getEmpresas)
app.get('/api/v1/enterprises/:id', db.getEmpresaById)
app.post('/api/v1/enterprises', db.createEmpresa)
app.put('/api/v1/enterprises/:id', db.updateEmpresa)
app.delete('/api/v1/enterprises/:id', db.deleteEmpresa)

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})